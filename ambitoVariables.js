document.addEventListener('mouseup', function(e){
  console.log(`${e.pageX}, ${e.pageY}`);
  //console.log(e.pageX + ', ' + e.pageY);
});


var yosObj2 = {
	nombre: 'Yoselin',
	apellidos: 'Arzola Jimenez',
	email: 'yarzola@ids.com',
	getNombreCompleto: function () {
		let nombreCompleto = '';
		let yosObjInterno = this;

		function completar() {
			nombreCompleto = `Desde yosObj2 ${yosObjInterno.nombre} ${yosObjInterno.apellidos} ${100*300}`;

			return nombreCompleto;
		}
		
		return completar();
	}
};


class Persona {
	constructor(nombre, apellidos, email = 'algo') {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.email = email;
		/*if (typeof(mail) !== undefined) {
			this.email = email;	
		} else {
			this.email = 'algo';
		}*/
	}

	getNombreCompleto () {
		return `${this.nombre} ${this.apellidos} ${this.email}`;
	}
}

const per = new Persona('Jaime', 'Cervantes Velasco');
console.log(per);
console.log(per.getNombreCompleto());

var nc = yosObj2.getNombreCompleto();
console.log(nc);



function elevados () {
	var elevado1 = undefined;
	console.log(elevado1);
	// console.log(elevado2);

	var elevado1 = 'ELEVADO 1';
	let elevado2 = 'ELEVADO 2';
}

elevados();

function saludo(){
	alert('Hola mundo');
}


const btnSubmit = document.getElementById('submit')

/*
btnSubmit.onclick = function (e) {
	console.log(e);
};
*/
let miPrimerCustomEvent = new CustomEvent('miPrimerCustomEvent', { detail: yosObj2, bubbles: true });

btnSubmit.addEventListener('click', function (e) {
	e.preventDefault();

	btnSubmit.dispatchEvent(miPrimerCustomEvent);
	console.log(e);
});


document.querySelector('main').addEventListener('miPrimerCustomEvent', function (e) {
	console.log(e);
});

document.addEventListener('mouseup', function(e){
  console.log(`${e.pageX}, ${e.pageY}`);
  //console.log(e.pageX + ', ' + e.pageY);
});


